;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Extraction of Alpha Beta Pruning
;Authors: Ulrich Berger and Monika Seisenberger
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Formalisation in the Minlog system: see www.minlog-system.de
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(begin
(load "~/minlog/init.scm")
(set! COMMENT-FLAG #f)
(libload "nat.scm") ;loaded by int.scm
(libload "pos.scm") ;loaded by int.scm
(libload "int.scm") ;introduces integer type and basic operations, ~ is negation symbol
(set! COMMENT-FLAG #t)  


(define nat (py "nat"))
(define state (py "nat"))
(define index (py "nat"))
(define value (py "int"))
(define stage (py "nat"))


(remove-var-name "i" "j" "k") ; was type int in library
(add-var-name "x" "y" state)
(add-var-name "n" stage)
(add-var-name "i" "j" "k" index)
(add-var-name "a" "b" value)
(add-var-name  "v" "w" value)


;; len x = length of x = number of children of x, len x = 0 means x is terminal
;; chi x i = i-th child of x (i < len x)
;; eva x = heuristic value of state, only needed for terminal states
(add-program-constant "len" (mk-arrow state index) t-deg-one)
(add-program-constant "chi" (mk-arrow state index state) t-deg-one)
(add-program-constant "eva" (mk-arrow state value) t-deg-one)


;; minfty and pinfty are extremal values
(add-program-constant "minfty" value t-deg-one)
(add-program-constant "pinfty" value t-deg-one)
(add-global-assumption "Ax-Minfty" "all v(minfty <= v)")
(add-global-assumption "Ax-Pinfty" "all v(v <= pinfty)")
(add-rewrite-rule (pt "minfty<=v")(pt "T"))
(add-rewrite-rule (pt "v<minfty")(pt "F"))


;Definition of Attacker Function
(apc "AttF" (mk-arrow nat state value) t-deg-one)
(aga "Ax-AttF0" "all x. (AttF Zero x = eva x)") ;can be proven
(aga "Ax-AttFlen0" "all n,x. len x = Zero -> (AttF n x = eva x)")
;AttF (n+1) x is maximum/minimum of all children: not needed as function!

					;

;; Simulaneous nductive definitions of attacker and defender relations
;;(add-ids (list (list "AttR" (make-arity nat state value))
;;               (list "DefR" (make-arity nat state value)))
;;	 '("all x(AttR 0 x (eva x))" "GenAttEva0")
;;	 '("all n,x(len x=0  -> AttR n x (eva x))" "GenAttEvaLen")
;;         '("all n,x,v,i (0 < n 
;;                       -> i<len x 
;;                       -> DefR (n--1) (chi x i) v 
;;                       -> all j(j<len x -> DefF (n--1) (chi x j) <= v)
;;                       -> AttR n x v)" "GenAttChi")
;;	 '("all x(DefR 0 x (eva x))" "GenDefEva0")
;;	 '("all n,x(len x=0  -> DefR n x (eva x))" "GenDefEvaLen")
;;         '("all n,x,v,i (0 < n 
;;                       -> i<len x 
;;                       -> AttR (n--1) (chi x i) v 
;;                       -> all j(j<len x -> v <= AttF (n--1) (chi x j))
;;                       -> DefR n x v)" "GenDefChi"))
;;used in first version of proofs.

;; Inductive definitions of attacker/defender relation
(add-ids (list (list "AttR" (make-arity nat state value) "algAtt"))
         '("all x (AttR Zero x (eva x))" "GenAttREva")
         '("all n,x(len x=Zero  -> AttR n x (eva x))" "GenAttREvaLen") 
	 '("all n,x,v,i (Zero < n 
                       -> i<len x 
                       -> AttR (n--1) (chi x i) (~ v) 
                       -> all j(j<len x -> ~ (AttF (n--1) (chi x j)) <= v)
                       -> AttR n x v)" "GenAChi"))


;; 







;AttFk is a supporting definition, and find maxium value up to the k-th child, basically defined recursively
;k <= len x implicit, [if k> len x, then set AttFk to pinfty
(apc "AttFk" (mk-arrow nat nat state value) t-deg-one)
;base
(add-computation-rule (pt "AttFk Zero n x")(pt "minfty")) 
;step
(add-computation-rule (pt "AttFk (Succ k) n x")
                      (pt "[if (~ (AttF n (chi x k)) < AttFk k n x)
		            (AttFk k n x)
		            (~ (AttF n (chi x k)))]"))

;if k=len x, by definition AttFk = AttF at the next level
(aga "AttFkF" "all n,x ( Zero <len x -> AttF (Succ n) x = AttFk (len x) n x)") 

;connection between Att function and relation - for convenience both directions
(aga "Ax-AttRF" "all n,x,v (AttR n x v -> v = AttF n x)")
(aga "Ax-AttRF2" "all n,x,v (AttR n x v -> AttF n x = v)")

;two basic properties of AttFk, follow directly from the definition
(aga "AttFAttFk" "all k,n,x,j (j<k -> ~ (AttF n (chi x j)) <= AttFk k n x)")
(aga "AttFk-inc" (pf "all k,n,x. AttFk k n x <= AttFk (Succ k) n x"))
;note missing is: k <= len x everywhere
;(aga "AttFk-inc" (pf "all k,n,x. k+1 <= len x ->  AttFk k n x <= AttFk (Succ k) n x"))


; basic properties, can be easily proven, using int library
(aga "Lemma~" "all w1,w2. w1 < ~ w2 -> w2 < ~ w1")
(aga "Lemma2~" "all w1,w2. ~ w1 < w2 -> ~ w2 <  w1")
(aga "Lemma3~" "all w1,w2. w1 = w2 -> ~ w1 = ~ w2")
(aga "Lemma4~" "all w1,w2. w1 <= ~ w2 -> w2 <= ~ w1")
(aga "Lemma5~" "all w1,w2. ~ w1 <= w2 -> ~ w2 <=  w1")

;partly contained in int library, or easy consequences
(aga "lemma0" "all m ((m=Zero->F) -> Zero<m)") 
(aga "lemma1" "all i,m (i<m ->i<m+1)")  
(aga "lemma2" "all j,m (j < m+1 -> (j<m ->F) -> j=m)") 
(aga "lemmaintweakening" "all w1,w2 (w1<w2 -> w1<=w2)")
(aga "lemmaintweakening2" "all w1,w2 (w1=w2 -> w1<=w2)") 
(aga "absurdint"  "all w1,w2( w1<=w2 -> w2<w1 -> F)")
(aga "absurdint3"  "all w0,w1,w2 (w2< w1 -> w1< w0 -> w0<=w2 -> F)")
(aga "lemmaint0" "all w1,w2 ((w1<w2->F) -> w1<=w2 -> w2=w1)") 
(aga "lemmaint3" "all w1,w2. (w1<w2 -> F) -> w2<=w1") 
(aga "lemmaint6" "all v0,v1 (( ~ v0<=v1 ->F) -> v1 < ~ v0)")
(aga "transint2" "all w0,w1,w2 (w0<=w2 -> w0=w1 -> w1 <= w2)")
(aga "transint3" "all b,w2,w1 (b< w2 -> w2<w1  -> b<w1)")
(aga "transint4" "all a,b, v1( v1<a ->a<=b -> b<v1 -> F)")
(aga "lemmaint7" "all v0,v1 (( v0<=v1 ->F) -> v1<v0)")
(aga "lemmaint5" "all w1,w2,w3 (w3<=w1 -> w1<w2 -> w3 <= w2)")
(aga "transint5" "all v0,a,v1 (v0<a ->a<=v1 -> v0<v1)")
(aga "transint6" "all v0,v1,a (v0<=v1 -> v1<a  -> v0<a)")




);matches def begin


;;pruning



(begin
(set-goal "all n,x,a,b (a <= b ->
ex v ((a<=v ->v<=b -> AttR n x v) &
      (v<a -> AttF n x  < a)    &
      (b<v -> b< AttF n x)))")



(ind);Induction on n.

;;base 

(assume "x" "a" "b" "a<=b")
(ex-intro (pt "eva x"))
(split)
(assume 2 3)
(intro 0)
(split)
(assume 2)
(inst-with-to "Ax-AttF0" (pt "x") "Ax-AttF0-inst")
(simp "Ax-AttF0")
(use 2)
(assume 2)
(inst-with-to "Ax-AttF0" (pt "x") "Ax-AttF0-inst")
(simp "Ax-AttF0")
(use 2)


;step

(assume "n" "ih" "x" "a" "b")
(cases (pt "len x = Zero"))

;;case len x = 0

(assume "len x = Zero")
(assume 4)
;;left (Att)
(ex-intro "eva x")
(split)
;a<=eva x -> eva x<=b -> AttR(Succ n)x(eva x)
(assume 2 3)
(intro 1)
(use "len x = Zero")
(split)
(inst-with-to "Ax-AttFlen0" (pt "n+1")(pt "x") "len x = Zero" "inst")
(simp "inst")
(assume 5)
(use 5)
(inst-with-to "Ax-AttFlen0" (pt "n+1")(pt "x") "len x = Zero" "inst")
(simp "inst")
(assume 5)
(use 5)


;;case 2: not len x = Zero
(assume "notlen0")
(assume "a<=b")
(inst-with-to "lemma0" (pt "len x") "notlen0" "Zero<len x")
(drop "notlen0")


;; cut Att - to add: k < len x
(cut "all k
ex v ((a<=v ->v<=b ->
       ((k = Zero -> v = minfty) 
        &
        (Zero < k -> ex i  (i < k &  AttR n (chi x i)(~ v))     
	     	     & all j (j < k ->  ~ (AttF n (chi x j)) <= v))))
     & (v<a -> AttFk k n x  < a)     
     & (b<v -> b<AttFk k n x))")

(assume "cutA")
;;cutA -> goal
(inst-with-to "cutA" (pt "len x") "cutAinst")
(drop "cutA")
(by-assume-with "cutAinst" "v" "v-prop")
(ex-intro (pt "v"))
(split)
(assume 8 9)
(inst-with-to "v-prop" 'left 8 9 'right "Zero<len x" 'left "v-propleft" )
(by-assume-with "v-propleft" "i" "i-prop")


(intro 2 (pt "i"))
(ng)
(use "Truth-Axiom")
(use "i-prop")
(ng)
(use "i-prop")
(ng)
(use "v-prop")
(use 8)
(use 9)
(use "Zero<len x")
;new, but the same
;          (v<a -> AttFk(len x)n x<a) & (b<v -> b<AttFk(len x)n x)
;-----------------------------------------------------------------------------
;(v<a -> AttF(Succ n)x<a) & (b<v -> b<AttF(Succ n)x)
					

(split)
(assume 2)
(inst-with-to "AttFkF" (pt "n")(pt "x") "Zero<len x" "AttFkFinst")
(simp "AttFkFinst")
(use "v-prop")
(use 8)
(assume 10)
(inst-with-to "AttFkF" (pt "n")(pt "x") "Zero<len x" "AttFkFinst")
(simp "AttFkFinst")
(use "v-prop")
(use 8)


;;proof of cutA
(ind)
;k=0
(ng)

(ex-intro (pt "minfty"))
(split)
(assume 5 6)
(split)
(ng)
(prop)
(prop)




(split)
(ng)
(prop)
(ng)
(prop)




;;step
(assume "k" "ihk")
(by-assume-with "ihk" "v1" "v1-prop")

;;CASE 1 v1<=b
(cases (pt "v1 <= b"))
(assume "v1<=b")

;;CASE 11 a<v1
(cases (pt "a<=v1"))
(assume "a<=v1")

(assert "~b <= ~v1")
(use "v1<=b")

(assume "~b<=~v1")


(inst-with-to "ih" (pt "chi x k")(pt "~ b ")(pt "~ v1") "~b<=~v1" "ihinst")
;(inst-with-to "ihinst" 'left "ihinstl")
;(inst-with-to "ihinst" 'right "ihinstr")
(drop "ih")
(by-assume-with "ihinst" "v0" "v0-prop")

;;CASE 111 0<k
(cases (pt "Zero < k"))
(assume "Zero<k")


(cases (pt "~v0<=v1"))



;;;CASE 1.1
; case ~v0<=v1: take v1 in this case.
(assume "~v0<=v1")

(ex-intro (pt "v1"))
(split)
;left
(assume 12 13)

(split)
(ng)
(prop)
(assume "Zero<Succ k")
(inst-with-to "v1-prop" 'left 14 15 'right "Zero<k" "v1-propinst")
(inst-with-to "v1-propinst" 'left "v1-propinstleft")
(inst-with-to "v1-propinst" 'right "v1-propinstright")

(by-assume-with "v1-propinstleft" "i" "i-prop")

(split)
(ex-intro (pt "i"))


(split)

;statement 1
;"lemma1": "all i,m (i<m ->i<m+1)"
(use "lemma1")
(use "i-prop")
(use "i-prop")

;jstatement
(assume "j" "j<Succ k")
(cases (pt "j<k"));done
;case j<k
(assume "j<k")

(use "v1-propinstright")
(use "j<k")

;case notj<k
(assume "notj<k")
;(aga "lemma2" "all j,m (j < m+1 -> (j<m ->F) -> j=m)")
(inst-with "lemma2" (pt "j")(pt "k") "j<Succ k" "notj<k")
(simp 23)

(cases (pt "~v0<v1"));truely smaller
;; case 1.1.1 true
(assume "~v0<v1");then 3rd statement of v0-prop can be used
;(aga "lemmaintweakening" "all w1,w2 (w1<w2 -> w1<=w2)")
(use "lemmaintweakening")

(use "Lemma2~")

(use "v0-prop")
(use "Lemma2~")
(use "~v0<v1")


; 1.1.2 false 

(assume "not~v0<v1")
;(aga "lemmaint0" "all w1,w2 ((w1<w2->F) -> w1<=w2 -> w2=w1)")
(inst-with-to "lemmaint0" (pt "~v0")(pt "v1") "not~v0<v1" "~v0<=v1" "inst")
(simp "inst")


;(aga "lemmaintweakening2" "all w1,w2 (w1=w2 -> w1<=w2)")
(use "lemmaintweakening2")


(inst-with-to "Ax-AttRF2" (pt "n")(pt "chi x k")(pt "v0") "inst2")
(use "Lemma3~")
(use "inst2")
(use "v0-prop")




(use "Lemma5~")


;(aga "transint2" "all w0,w1,w2 (w0<=w2 -> w0=w1 -> w1 <= w2)")
(use "transint2" (pt "v1"))

(use "v1<=b")
(use "inst")

;v0<= ~v1
(use "Lemma4~")

;(aga "lemmaint3" "all w1,w2. (w1<w2 -> F) -> w2<=w1")
(use "lemmaint3")
(use "not~v0<v1")


(split)

;;Statement 2 --note we are in case (a<v1)!
;?^165:v1<a -> AttFk(Succ k)n x<a
(assume "v1<a")
(aga "absurdint"  "all w1,w2( w1<=w2 -> w2<w1 -> F)")
(inst-with "absurdint" (pt "a")(pt "v1") "a<=v1" "v1<a")
(prop)


;;statement 3
(assume "b<v1")
(inst-with "absurdint" (pt "v1")(pt "b") "v1<=b" "b<v1")
(prop)
; proved




;;CASE 1.2 not ~v0<=v1
(assume "not~v0<=v1")
;(aga "lemmaint6" "all v0,v1 (( ~ v0<=v1 ->F) -> v1 < ~ v0)")
(inst-with-to "lemmaint6" (pt "v0")(pt "v1") "not~v0<=v1" "v1<~v0")
(drop "not~v0<=v1")

;CASE 1.2.1
(cases (pt "~v0<=b"))
(assume "~v0<=b")
(ex-intro (pt "~v0"))
(split)
(assume "a<=v0" "~v0<=bobsolete")
(split)
(ng)
(prop)
(assume "obsolete")
(split)
(ex-intro (pt "k"))
(split)
(use "Truth-Axiom")

(use "v0-prop")

;(aga "lemmaint5" "all w1,w2 (w1<w2 -> w1<=w2)")



(use "Lemma5~")
(use "~v0<=b")
(use "Lemma4~")


(use "lemmaintweakening")
(use "v1<~v0");wrong name
;proved

;;j case
(assume "j" "j<Succ k")
(cases (pt "j<k"))
(assume "j<k")
;(aga "lemmaint5" "all w1,w2,w3 (w3<=w1 -> w1<w2 -> w3 <= w2)")
(use "lemmaint5" (pt "v1"))

;(inst-with-to "lemma5" (pt "a")(pt "v1") "a<v1" "a<=v1")
;;note v1-prop has wrong brackets, part unneeded
(inst-with-to "v1-prop" 'left "a<=v1" "v1<=b" 'right "Zero<k" "inst")
(inst-with-to  "inst" 'right "inst-right")


;(inst-with-to "v1-prop" 'left "a<=v1" "v1<=b" 'right "Zero<k" "inst")
;(by-assume-with "inst" "i" "i-prop")
;(use "i-prop")

(use "inst-right")
(use "j<k")
(use "v1<~v0")


;;case not j<k
(assume "notj<k")
(aga "lemma2" "all j,m (j < m+1 -> (j<m ->F) -> j=m)")
(inst-with-to "lemma2" (pt "j")(pt "k") "j<Succ k" "notj<k" "j=k")
(simp "j=k")

(use "lemmaintweakening2")

(use "Lemma3~")

;(aga "Ax-AttRF" "all n,x,v (AttR n x v -> v = AttF n x)")
(use-with "Ax-AttRF2" (pt "n")(pt "chi x k")(pt "v0") "?")
(use "v0-prop")

(use "Lemma5~")
(use "~v0<=b")
(use "Lemma4~")
(use "lemmaintweakening")

(use "v1<~v0")


;statement 2
(split)
(assume "~v0<a")





;(aga "absurdint3"  "all w0,w1,w2 (w2< w1 -> w1< w0 -> w0<=w2 -> F)")
(inst-with-to "absurdint3" (pt "a")(pt "~v0")(pt "v1") "v1<~v0" "~v0<a" "a<=v1" "F")
(prop)
;statement 3
(assume "b<~v0")
;(aga "absurd"  "all m,n( m<=n -> n<m -> F)")
(inst-with-to "absurdint" (pt "~v0")(pt "b") "~v0<=b" "b<~v0" "F")
(prop)




;CASE 1.2.2 not~v0<=b
(assume "not~v0<=b")
;(aga "lemmaint6" "all v0,v1 (( ~ v0<=v1 ->F) -> v1 < ~ v0)")
(inst-with-to "lemmaint6" (pt "v0")(pt "b") "not~v0<=b" "b<~v0")
(drop "not~v0<=b")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(ex-intro (pt "~v0"))
(split)
(assume "dummy" "~v0<=b")
(inst-with-to "absurdint" (pt "~v0")(pt "b") "~v0<=b" "b<~v0" "F")
(prop)
;statement 2
(split)
(assume "~v0<a")
;(aga "absurdint3"  "all w,v0,v1 (v1< v0 -> v0< w -> w <=v1 -> F)")
(inst-with-to "absurdint3" (pt "a")(pt "~v0")(pt "v1") "v1<~v0" "~v0<a" "a<=v1" "F")
(prop)
;statement 3
(assume "b<v0dummy")
(drop "b<v0dummy")


(cut "v0 < ~ b")
(assume "v0<~b")


(inst-with-to "v0-prop" 'right 'left "v0<~b" "AttF-prop")


(ng)
(cases (pt "(~ (AttF n (chi x k))<AttFk k n x)"))


(assume "AttFsmaller")
(ng)




;(aga "transint3" "all b,w2,w1 (b< w2 -> w2<w1  -> b<w1)")
(use "transint3" (pt "~ (AttF n (chi x k))"))
(use "Lemma~")
(use "AttF-prop")

(use "AttFsmaller"); was Def smller needed?
;false
(ng)
(assume "AttFsmaller")

(use "Lemma~")
(use "AttF-prop")
;cut
(use "Lemma~")
(use "b<~v0")
;done 






;k=0
(assume "not0<k")
(aga "lemma00" "all k ((Zero<k->F) -> k=Zero)")
(inst-with-to "lemma00" (pt "k") "not0<k" "k=Zero")
(drop "not0<k")

(ex-intro (pt "~v0"))
(split)
(assume "a<=~v0" "~v0<=b")
(split)
(ng)
(prop)
(assume "triv")
(split)
(ex-intro (pt "Zero"))



(split)
;left
(use "Truth-Axiom")
;(split)
;middle




(simp "<-" "k=Zero")
(ng) ;~~v0->v0

(use "v0-prop")
(use "Lemma5~")
(use "~v0<=b")
(use "Lemma4~")




(inst-with-to "v1-prop" 'left "a<=v1" "v1<=b" 'left "k=Zero" "v1=minfty")

(simp "v1=minfty")


(aga "ax-minfty0" "all v0. minfty<=v0")

(use "ax-minfty0")


;right
(assume "j")
(simp "k=Zero")
(assume "j<1")
(aga "lemma12" "all j (j<1 -> j=Zero)")
(inst-with-to "lemma12" (pt "j") "j<1" "j=Zero")
(simp "j=Zero")
(simp "<-" "k=Zero")
(use "lemmaintweakening2")

(use "Lemma3~")


(use-with "Ax-AttRF2" (pt "n")(pt "chi x k")(pt "v0") "?")


(use-with "v0-prop" 'left "?" "?")
;~b<=v0
(use "Lemma5~")
(use "~v0<=b")


(inst-with-to "v1-prop" 'left "a<=v1" "v1<=b" 'left "k=Zero" "v1=minfty")
(simp "v1=minfty")
(use "Lemma4~")
(use "ax-minfty0")




;statement2
(split)
(assume "~v0<a")
(simp "k=Zero")
(ng)

;(add-rewrite-rule (pt "n<minfty")(pt "F"))
(ng)
;?^323:DefF n(chi x 0)<a
;from v0-prop we know that DefFn (chi x k) < v1



(simp "<-" "k=Zero")
(aga "transint5" "all v0,a,v1 (v0<a ->a<=v1 -> v0<v1)")
(inst-with-to "v1-prop" 'left "a<=v1" "v1<=b" 'left "k=Zero" "v1=minfty")


(inst-with-to "transint5" (pt "~v0")(pt "a")(pt "v1") "~v0<a" "a<=v1"
	      "~v0<v1")


					;(inst-with-to "v0-prop" 'right 'left "v0<v1" "inst")
;(use "transint4" (pt "v1"))  
;(use "inst")
;(use "v1<=a") ;arg habe nur a<=v1


(simphyp "~v0<v1" "v1=minfty")
(ng)
(prop)


;statement 3
(assume "b<~v0")
(simp "k=Zero")
(ng)
(simp "<-" "k=Zero")
(use "Lemma~")
(use "v0-prop")
(use "Lemma~")
(use "b<~v0")

;new version works until here

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;CASE 12
;case v1<a
(assume "nota<=v1")
;(aga "lemmaint7" "all v0,v1 (( v0<=v1 ->F) -> v1<v0)")
(inst-with-to "lemmaint7" (pt "a")(pt "v1") "nota<=v1" "v1<a")
(drop "nota<=v1")

(assert "~ b <= ~ a")
(ng)
(use "a<=b")

(inst-with-to "ih" (pt "chi x k")(pt "~ b")(pt "~ a") "a<=b" "ihinst")
;(inst-with-to "ihinst" 'left "ihinstl")
;(inst-with-to "ihinst" 'right "ihinstr")
;(drop "ih" "ihinst" "ihinstl")
;(drop "ih")
(by-assume-with "ihinst" "v0" "v0-prop")



(cases (pt "Zero < k"))
(assume "Zero<k")
(cases (pt "~v0<=v1"))



; case ~v0<=v1: take v1 in this case.
(assume "~v0<=v1")
(assume "~b<=~a")
(ex-intro (pt "v1"))
(split)

;statement 1- absurd
(assume "a<=v1" "v1<=bdummy")
(inst-with-to  "absurdint" (pt "a")(pt "v1")  "a<=v1" "v1<a" "F")
(prop)


;statement 2
(split)
(assume "v1<aobsolete")
(ng)

(cases (pt "~((AttF n(chi x k)))<AttFk k n x"))

(assume "AttFsmaller")
(ng)
(use "v1-prop")
(use "v1<a")
(assume "AttFKsmaller")
(ng)
(use "Lemma2~")
(use "v0-prop")
(use "Lemma2~")					;~a<v0



;(aga "transint6" "all v0,v1,a (v0<=v1 -> v1<a  -> v0<a)")

(use "transint6" (pt "v1"))
(use "~v0<=v1")
(use "v1<a")


;statement 3
(assume "b<v1")
;(aga "transint4" "all a,b, v1( v1<a ->a<=b -> b<v1 -> F)")
(use "Efq")
(use-with "transint4" (pt "a")(pt "b")(pt "v1") "v1<a" "?" "b<v1")
(use "a<=b")


;not v0<=v1: i.e. v1 < ~v0
(assume "not~v0<=v1")
(assume "~b<=~a")
(inst-with-to "lemmaint7" (pt "~v0")(pt "v1") "not~v0<=v1" "v1<~v0")
(ex-intro (pt "~v0"))
(split)



(assume "a<=~v0" "~v0<=b")
;statement1
(split)
(ng)
(prop)



;right
(assume "dummy")

(split)					;
;take i=k
(ex-intro (pt "k"))
(split)
(use "Truth-Axiom")

(ng)


(use "v0-prop")
(use "Lemma5~")
(use "~v0<=b")
(use "Lemma4~")
(use "a<=~v0")



;;Case v1<a; v1 < ~v0
;j statement to prove
      

(assume "j" "j<Succ k")
(cases (pt "j<k"))
(assume "j<k")
;(aga "AttFAttFk" "all k,n,x,j (j<k -> ~ (AttF n (chi x j)) <= AttFk k n x)")

;goal ~(AttF n(chi x j))<= ~v0
;idea:
;~(AttF n(chi x j))<= AttFk k n x <  a <= ~v0
; because of: Lemma and  v1 prop (v1 <a) and a<=~v0     

(aga "transint8" "all b,a,v0 (b<=a  ->  a<=v0 ->    b <= v0)")
(use-with "transint8" (pt "~(AttF n(chi x j))") (pt "a")(pt "~ v0") "?" "a<=~v0")
; ~(AttF n(chi x j))<=a
(use-with "lemmaint5" (pt "(AttFk k n x)")(pt "a")(pt "~ (AttF n(chi x j))") "?" "?")
(use "AttFAttFk")
(use "j<k")

(use "v1-prop")
(use "v1<a")

;case j<k -> F
(assume "not")
(inst-with-to "lemma2" (pt "j")(pt "k") "j<Succ k" "not" "j=k")
(drop "not")
(simp "j=k")
					;~AttF n(chi x k)<=~v0
(ng)
(use "lemmaintweakening2")
(use-with "Ax-AttRF" (pt "n")(pt "chi x k")(pt "v0") "?")
(use "v0-prop")
(use "Lemma5~")
(use "~v0<=b")
(use "Lemma4~")
(use "a<=~v0")


(split)
;statement2 v1<v0, v0<a -> v1<a,  0<k
(assume "~v0<a")
(ng)
(cases (pt "~(AttF n(chi x k))<AttFk k n x"))

(assume "AttFsmaller")
(ng)
(use "v1-prop")
(use "v1<a")
;false
(assume "AttFnotsmaller")
(ng)

(use "Lemma2~")

(use "v0-prop")
(use "Lemma2~")
(use "~v0<a")

;statement3
(assume "b<~v0")
(ng)
(cases (pt "~ (AttF n(chi x k))<AttFk k n x"))
(assume "AttFsmaller")
(ng)
(use "Efq")
(inst-with-to "v1-prop" 'right 'left "v1<a" "AttFk<a")

(inst-with-to "transint3" (pt "~ (AttF n (chi x k))")(pt "AttFk k n x")(pt "a")
	      "AttFsmaller" "AttFk<a" "AttF<a")

;(aga "transint4" "all a,b, v1( v1<a ->a<=b -> b<v1 -> F)")
(use-with "transint4" (pt "a")(pt "b") (pt "~ (AttF n(chi x k))") "AttF<a" "?" "?")
(use "a<=b")
(use "Lemma~")

(use "v0-prop")
(use "Lemma~")
(use "b<~v0")
;
(assume "Attnotsmaller")
(ng)
(use "Lemma~")
(use "v0-prop")
(use "Lemma~")
(use "b<~v0")


;k=0

(assume "not0<k")
(assume "~b<=~a")
(aga "lemma00" "all k ((Zero<k->F) -> k=Zero)")
(inst-with-to "lemma00" (pt "k") "not0<k" "k=Zero")
(drop "not0<k")


(ex-intro (pt "~v0"))
(split)
(assume "a<=~v0" "~v0<=b")
(split)
(ng)
(prop)
(assume "triv")
(split)
(ex-intro (pt "Zero"))
(split)
;left
(use "Truth-Axiom")
;right
(simp "<-" "k=Zero")
(ng) ;red ~~


(use "v0-prop")
(use "Lemma5~")
(use "~v0<=b")
(use "Lemma4~")
(use "a<=~v0")


;jcase
(simp "k=Zero")
(assume "j" "j<1")

(inst-with-to "lemma12" (pt "j") "j<1" "j=Zero")
(simp "j=Zero")
(simp "<-" "k=Zero")
(ng)
(use "lemmaintweakening2")
(use-with "Ax-AttRF" (pt "n")(pt "chi x k")(pt "v0") "?")
(use "v0-prop")
(use "Lemma5~")
(use "~v0<=b")
(use "Lemma4~")
(use "a<=~v0")

;statement 2
(split)
(assume "~v0<a")
(ng)
(simp "k=Zero")
(ng)
(simp "<-" "k=Zero")
(use "Lemma2~")
(use "v0-prop")
(use "Lemma2~")
(use "~v0<a")
;statement3
(assume "b<~v0")
(simp "k=Zero")
(ng)
(simp "<-" "k=Zero")
(use "Lemma~")
(use "v0-prop")
(use "Lemma~")
(use "b<~v0")



;v1<b
(assume "not")
(inst-with-to "lemmaint7" (pt "v1")(pt "b") "not" "b<v1")

(ex-intro (pt "v1"))
(split)
(assume "a<=v1" "v1<=b")
(use "Efq")
(use-with "absurdint" (pt "v1")(pt "b") "v1<=b" "b<v1")
;statement2
(split)
(assume "v1<a")
(use "Efq")
(use-with "transint4" (pt "a")(pt "b")(pt "v1") "v1<a" "?" "b<v1")

(use "a<=b")
;statement3
(assume "b<v1dummy")

;(aga "AttFk-inc" (pf "all k,n,x. AttFk k n x <= AttFk (Succ k) n x"))
(use "transint5" (pt "AttFk k n x"))
(use "v1-prop")
(use "b<v1")
(use "AttFk-inc")




;(save  "alpha-beta")
);matches proof begin



(begin
(define stored-proof (current-proof))
(define program (proof-to-extracted-term stored-proof))
(dt (nt program))

;extract program of abn11 version - only left part
[x0](Rec nat=>nat=>int=>int=>int)x0([x4,a5,a6]eva x4)([x4,(nat=>int=>int=>int)_5,x6,a7,a8][if (len x6=Zero) (eva x6) (left((Rec nat=>int@@nat)(len x6)(minfty@Zero)([x9,(int@@nat)_10][if (left(int@@nat)_10<=a8) [if (a7<=left(int@@nat)_10) [if (Zero<x9) [if (~((nat=>int=>int=>int)_5(chi x6 x9)~a8~left(int@@nat)_10)<=left(int@@nat)_10) (int@@nat)_10 (~((nat=>int=>int=>int)_5(chi x6 x9)~a8~left(int@@nat)_10)@[if (~((nat=>int=>int=>int)_5(chi x6 x9)~a8~left(int@@nat)_10)<=a8) x9 Zero])] (~((nat=>int=>int=>int)_5(chi x6 x9)~a8~left(int@@nat)_10)@Zero)] [if (Zero<x9) [if (~((nat=>int=>int=>int)_5(chi x6 x9)~a8~a7)<=left(int@@nat)_10) (left(int@@nat)_10@Zero) (~((nat=>int=>int=>int)_5(chi x6 x9)~a8~a7)@x9)] (~((nat=>int=>int=>int)_5(chi x6 x9)~a8~a7)@Zero)]] (left(int@@nat)_10@Zero)])))]

[n0](Rec nat=>nat=>int=>int=>int@@algAtt)n0([n4,a5,a6]eva n4@CGenAttREva n4)([n4,(nat=>int=>int=>int@@algAtt)_5,n6,a7,a8][if (len n6=Zero) (eva n6@CGenAttREvaLen(Succ n4)n6) (left((Rec nat=>int@@nat@@algAtt)(len n6)(minfty@Zero@CGenAttREva Zero)([n9,(int@@nat@@algAtt)_10][if (left(int@@nat@@algAtt)_10<=a8) [if (a7<=left(int@@nat@@algAtt)_10) [if (Zero<n9) [if (~left((nat=>int=>int=>int@@algAtt)_5(chi n6 n9)~a8~left(int@@nat@@algAtt)_10)<=left(int@@nat@@algAtt)_10) (int@@nat@@algAtt)_10 (~left((nat=>int=>int=>int@@algAtt)_5(chi n6 n9)~a8~left(int@@nat@@algAtt)_10)@[if (~left((nat=>int=>int=>int@@algAtt)_5(chi n6 n9)~a8~left(int@@nat@@algAtt)_10)<=a8) (n9@right((nat=>int=>int=>int@@algAtt)_5(chi n6 n9)~a8~left(int@@nat@@algAtt)_10)) (Zero@CGenAttREva Zero)])] (~left((nat=>int=>int=>int@@algAtt)_5(chi n6 n9)~a8~left(int@@nat@@algAtt)_10)@Zero@right((nat=>int=>int=>int@@algAtt)_5(chi n6 n9)~a8~left(int@@nat@@algAtt)_10))] [if (Zero<n9) [if (~left((nat=>int=>int=>int@@algAtt)_5(chi n6 n9)~a8~a7)<=left(int@@nat@@algAtt)_10) (left(int@@nat@@algAtt)_10@Zero@CGenAttREva Zero) (~left((nat=>int=>int=>int@@algAtt)_5(chi n6 n9)~a8~a7)@n9@right((nat=>int=>int=>int@@algAtt)_5(chi n6 n9)~a8~a7))] (~left((nat=>int=>int=>int@@algAtt)_5(chi n6 n9)~a8~a7)@Zero@right((nat=>int=>int=>int@@algAtt)_5(chi n6 n9)~a8~a7))]] (left(int@@nat@@algAtt)_10@Zero@CGenAttREva Zero)]))@CGenAChi(Succ n4)n6 left((Rec nat=>int@@nat@@algAtt)(len n6)(minfty@Zero@CGenAttREva Zero)([n9,(int@@nat@@algAtt)_10][if (left(int@@nat@@algAtt)_10<=a8) [if (a7<=left(int@@nat@@algAtt)_10) [if (Zero<n9) [if (~left((nat=>int=>int=>int@@algAtt)_5(chi n6 n9)~a8~left(int@@nat@@algAtt)_10)<=left(int@@nat@@algAtt)_10) (int@@nat@@algAtt)_10 (~left((nat=>int=>int=>int@@algAtt)_5(chi n6 n9)~a8~left(int@@nat@@algAtt)_10)@[if (~left((nat=>int=>int=>int@@algAtt)_5(chi n6 n9)~a8~left(int@@nat@@algAtt)_10)<=a8) (n9@right((nat=>int=>int=>int@@algAtt)_5(chi n6 n9)~a8~left(int@@nat@@algAtt)_10)) (Zero@CGenAttREva Zero)])] (~left((nat=>int=>int=>int@@algAtt)_5(chi n6 n9)~a8~left(int@@nat@@algAtt)_10)@Zero@right((nat=>int=>int=>int@@algAtt)_5(chi n6 n9)~a8~left(int@@nat@@algAtt)_10))] [if (Zero<n9) [if (~left((nat=>int=>int=>int@@algAtt)_5(chi n6 n9)~a8~a7)<=left(int@@nat@@algAtt)_10) (left(int@@nat@@algAtt)_10@Zero@CGenAttREva Zero) (~left((nat=>int=>int=>int@@algAtt)_5(chi n6 n9)~a8~a7)@n9@right((nat=>int=>int=>int@@algAtt)_5(chi n6 n9)~a8~a7))] (~left((nat=>int=>int=>int@@algAtt)_5(chi n6 n9)~a8~a7)@Zero@right((nat=>int=>int=>int@@algAtt)_5(chi n6 n9)~a8~a7))]] (left(int@@nat@@algAtt)_10@Zero@CGenAttREva Zero)]))left right((Rec nat=>int@@nat@@algAtt)(len n6)(minfty@Zero@CGenAttREva Zero)([n9,(int@@nat@@algAtt)_10][if (left(int@@nat@@algAtt)_10<=a8) [if (a7<=left(int@@nat@@algAtt)_10) [if (Zero<n9) [if (~left((nat=>int=>int=>int@@algAtt)_5(chi n6 n9)~a8~left(int@@nat@@algAtt)_10)<=left(int@@nat@@algAtt)_10) (int@@nat@@algAtt)_10 (~left((nat=>int=>int=>int@@algAtt)_5(chi n6 n9)~a8~left(int@@nat@@algAtt)_10)@[if (~left((nat=>int=>int=>int@@algAtt)_5(chi n6 n9)~a8~left(int@@nat@@algAtt)_10)<=a8) (n9@right((nat=>int=>int=>int@@algAtt)_5(chi n6 n9)~a8~left(int@@nat@@algAtt)_10)) (Zero@CGenAttREva Zero)])] (~left((nat=>int=>int=>int@@algAtt)_5(chi n6 n9)~a8~left(int@@nat@@algAtt)_10)@Zero@right((nat=>int=>int=>int@@algAtt)_5(chi n6 n9)~a8~left(int@@nat@@algAtt)_10))] [if (Zero<n9) [if (~left((nat=>int=>int=>int@@algAtt)_5(chi n6 n9)~a8~a7)<=left(int@@nat@@algAtt)_10) (left(int@@nat@@algAtt)_10@Zero@CGenAttREva Zero) (~left((nat=>int=>int=>int@@algAtt)_5(chi n6 n9)~a8~a7)@n9@right((nat=>int=>int=>int@@algAtt)_5(chi n6 n9)~a8~a7))] (~left((nat=>int=>int=>int@@algAtt)_5(chi n6 n9)~a8~a7)@Zero@right((nat=>int=>int=>int@@algAtt)_5(chi n6 n9)~a8~a7))]] (left(int@@nat@@algAtt)_10@Zero@CGenAttREva Zero)]))right right((Rec nat=>int@@nat@@algAtt)(len n6)(minfty@Zero@CGenAttREva Zero)([n9,(int@@nat@@algAtt)_10][if (left(int@@nat@@algAtt)_10<=a8) [if (a7<=left(int@@nat@@algAtt)_10) [if (Zero<n9) [if (~left((nat=>int=>int=>int@@algAtt)_5(chi n6 n9)~a8~left(int@@nat@@algAtt)_10)<=left(int@@nat@@algAtt)_10) (int@@nat@@algAtt)_10 (~left((nat=>int=>int=>int@@algAtt)_5(chi n6 n9)~a8~left(int@@nat@@algAtt)_10)@[if (~left((nat=>int=>int=>int@@algAtt)_5(chi n6 n9)~a8~left(int@@nat@@algAtt)_10)<=a8) (n9@right((nat=>int=>int=>int@@algAtt)_5(chi n6 n9)~a8~left(int@@nat@@algAtt)_10)) (Zero@CGenAttREva Zero)])] (~left((nat=>int=>int=>int@@algAtt)_5(chi n6 n9)~a8~left(int@@nat@@algAtt)_10)@Zero@right((nat=>int=>int=>int@@algAtt)_5(chi n6 n9)~a8~left(int@@nat@@algAtt)_10))] [if (Zero<n9) [if (~left((nat=>int=>int=>int@@algAtt)_5(chi n6 n9)~a8~a7)<=left(int@@nat@@algAtt)_10) (left(int@@nat@@algAtt)_10@Zero@CGenAttREva Zero) (~left((nat=>int=>int=>int@@algAtt)_5(chi n6 n9)~a8~a7)@n9@right((nat=>int=>int=>int@@algAtt)_5(chi n6 n9)~a8~a7))] (~left((nat=>int=>int=>int@@algAtt)_5(chi n6 n9)~a8~a7)@Zero@right((nat=>int=>int=>int@@algAtt)_5(chi n6 n9)~a8~a7))]] (left(int@@nat@@algAtt)_10@Zero@CGenAttREva Zero)])))])> 
					  

					  
(term-to-expr (nt program))
;abn11 version					  
(((natRec x0)
     (lambda (x4) (lambda (a5) (lambda (a6) (eva x4)))))
    (lambda (x4)
      (lambda (lpar_nat=>int=>int=>int_rpar_5)
        (lambda (x6)
          (lambda (a7)
            (lambda (a8)
              (if (= (len x6) 0)
                  (eva x6)
                  (car (((natRec (len x6)) (cons minfty 0))
                         (lambda (x9)
                           (lambda (lpar_int@@nat_rpar_10)
                             (if (<= (car lpar_int@@nat_rpar_10) a8)
                                 (if (<= a7 (car lpar_int@@nat_rpar_10))
                                     (if (< 0 x9)
                                         (if (<= (IntUMinus
                                                   (((lpar_nat=>int=>int=>int_rpar_5
                                                       ((chi x6) x9))
                                                      (IntUMinus a8))
                                                     (IntUMinus
                                                       (car lpar_int@@nat_rpar_10))))
                                                 (car lpar_int@@nat_rpar_10))
                                             lpar_int@@nat_rpar_10
                                             (cons
                                               (IntUMinus
                                                 (((lpar_nat=>int=>int=>int_rpar_5
                                                     ((chi x6) x9))
                                                    (IntUMinus a8))
                                                   (IntUMinus
                                                     (car lpar_int@@nat_rpar_10))))
                                               (if (<= (IntUMinus
                                                         (((lpar_nat=>int=>int=>int_rpar_5
                                                             ((chi x6) x9))
                                                            (IntUMinus a8))
                                                           (IntUMinus
                                                             (car lpar_int@@nat_rpar_10))))
                                                       a8)
                                                   x9
                                                   0)))
                                         (cons
                                           (IntUMinus
                                             (((lpar_nat=>int=>int=>int_rpar_5
                                                 ((chi x6) x9))
                                                (IntUMinus a8))
                                               (IntUMinus
                                                 (car lpar_int@@nat_rpar_10))))
                                           0))
                                     (if (< 0 x9)
                                         (if (<= (IntUMinus
                                                   (((lpar_nat=>int=>int=>int_rpar_5
                                                       ((chi x6) x9))
                                                      (IntUMinus a8))
                                                     (IntUMinus a7)))
                                                 (car lpar_int@@nat_rpar_10))
                                             (cons
                                               (car lpar_int@@nat_rpar_10)
                                               0)
                                             (cons
                                               (IntUMinus
                                                 (((lpar_nat=>int=>int=>int_rpar_5
                                                     ((chi x6) x9))
                                                    (IntUMinus a8))
                                                   (IntUMinus a7)))
                                               x9))
                                         (cons
                                           (IntUMinus
                                             (((lpar_nat=>int=>int=>int_rpar_5
                                                 ((chi x6) x9))
                                                (IntUMinus a8))
                                               (IntUMinus a7)))
                                           0)))
                                 (cons
                                   (car lpar_int@@nat_rpar_10)
                                   0))))))))))))))

					  





);matches extraction begin

;Run extracted program with sample tree

(begin
(apc "zerozero" (py "nat") t-deg-one)
(apc "zeroone" (py "nat") t-deg-one)
(apc "zerotwo" (py "nat") t-deg-one)
(apc "zerothree" (py "nat") t-deg-one)
(apc "zerofour" (py "nat") t-deg-one)
(apc "zerofive" (py "nat") t-deg-one)
(apc "zerosix" (py "nat") t-deg-one)
(apc "zeroseven" (py "nat") t-deg-one)

(add-rewrite-rule (pt "len zerozero")(pt "Zero"))
(add-rewrite-rule (pt "len zeroone")(pt "Zero"))
(add-rewrite-rule (pt "len zerotwo")(pt "Zero"))
(add-rewrite-rule (pt "len zerothree")(pt "Zero"))
(add-rewrite-rule (pt "len zerofour")(pt "Zero"))
(add-rewrite-rule (pt "len zerofive")(pt "Zero"))
(add-rewrite-rule (pt "len zerosix")(pt "Zero"))
(add-rewrite-rule (pt "len zeroseven")(pt "Zero"))


(add-rewrite-rule (pt "eva zerozero")(pt "IntN 2"))
(add-rewrite-rule (pt "eva zeroone")(pt "IntN 3"))
(add-rewrite-rule (pt "eva zerotwo")(pt "IntN 5"))
(add-rewrite-rule (pt "eva zerothree")(pt "IntN 7"))
(add-rewrite-rule (pt "eva zerofour")(pt "IntN 1"))
(add-rewrite-rule (pt "eva zerofive")(pt "0"))
(add-rewrite-rule (pt "eva zerosix")(pt "IntN 6"))
(add-rewrite-rule (pt "eva zeroseven")(pt "IntN 9"))

;to check that labels 7,6,9 are not needed, remove program constant eva, and only
;add the rewriting rules for the labels you expect to be needed.
;(rpc "eva")
;(add-program-constant "eva" (mk-arrow state value) t-deg-one)


(apc "onezero" (py "nat") t-deg-one)
(apc "oneone" (py "nat") t-deg-one)
(apc "onetwo" (py "nat") t-deg-one)
(apc "onethree" (py "nat") t-deg-one)
(add-rewrite-rule (pt "len onezero")(pt "Succ (Succ Zero)"))
(add-rewrite-rule (pt "len oneone")(pt "Succ (Succ Zero)"))
(add-rewrite-rule (pt "len onetwo")(pt "Succ (Succ Zero)"))
(add-rewrite-rule (pt "len onethree")(pt "Succ (Succ Zero)"))
(add-rewrite-rule (pt "chi onezero Zero")(pt "zerozero"))
(add-rewrite-rule (pt "chi onezero (Succ Zero)")(pt "zeroone"))
(add-rewrite-rule (pt "chi oneone Zero")(pt "zerotwo"))
(add-rewrite-rule (pt "chi oneone (Succ Zero)")(pt "zerothree"))
(add-rewrite-rule (pt "chi onetwo Zero")(pt "zerofour"))
(add-rewrite-rule (pt "chi onetwo (Succ Zero)")(pt "zerofive"))
(add-rewrite-rule (pt "chi onethree Zero")(pt "zerosix"))
(add-rewrite-rule (pt "chi onethree (Succ Zero)")(pt "zeroseven"))


(apc "twozero" (py "nat") t-deg-one)
(apc "twoone" (py "nat") t-deg-one)
(add-rewrite-rule (pt "len twozero")(pt "Succ (Succ Zero)"))
(add-rewrite-rule (pt "len twoone")(pt "Succ (Succ Zero)"))
(add-rewrite-rule (pt "chi twozero Zero")(pt "onezero"))
(add-rewrite-rule (pt "chi twozero (Succ Zero)")(pt "oneone")) 
(add-rewrite-rule (pt "chi twoone Zero")(pt "onetwo"))
(add-rewrite-rule (pt "chi twoone (Succ Zero)")(pt "onethree")) 


(apc "threezero" (py "nat") t-deg-one)
(add-rewrite-rule (pt "len threezero")(pt "Succ (Succ Zero)"))
(add-rewrite-rule (pt "chi threezero Zero")(pt "twozero"))
(add-rewrite-rule (pt "chi threezero (Succ Zero)")(pt "twoone"))


(add-rewrite-rule (pt "minfty<=v")(pt "T"))
(add-rewrite-rule (pt "v<=pinfty")(pt "T"))
(add-rewrite-rule (pt "pinfty<v")(pt "F"))
(add-rewrite-rule (pt "v<minfty")(pt "F"))
;also needed for NEGMAX version!
(add-rewrite-rule (pt "~ minfty")(pt "pinfty"))
(add-rewrite-rule (pt "v <= ~ minfty")(pt "T"))
(add-rewrite-rule (pt "~ pinfty<= v")(pt "T"))


);matches example begin




(define test1 (nt (mk-term-in-app-form program (pt "(Succ Zero)") (pt "onezero")
				              (pt "minfty")(pt "pinfty"))))

(nt (dt test1))

;result:
;;3 @CGenAChi(Succ Zero)onezero 3(Succ Zero)(CGenAttREva zeroone)

;stage = 1, x = onezero, k = 1 --> move is right.

;;abn11 version -left component only
;;;3
;;;Result with positive Labels at Leaves: IntN 2
;;;Result in Attacher/Defender version: 3@2





(define test2 (nt (mk-term-in-app-form program
				       (pt "Succ (Succ (Succ Zero))")
				       (pt "threezero")  (pt "minfty")(pt "pinfty"))))


(nt (dt test2))
;result aabn12 version
3@CGenAChi(Succ(Succ(Succ Zero)))threezero 3 Zero(CGenAChi(Succ(Succ Zero))twozero IntN 3 Zero(CGenAChi(Succ Zero)onezero 3(Succ Zero)(CGenAttREva zeroone)))

; n=3, x = threezero, v= 3 k=0
; n=2, x = twozero, v=intN 3, k=0,
; n=1, x = onezero, v = 3, k=1

;;Result: 3
;;Result with Positive Lables  IntN 5
;;Result in Attacker/Defender version: 3@5

;;Todo: ;; 3) replace lemmas by library thms
